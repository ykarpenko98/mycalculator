//
//  ViewController.swift
//  Calculator
//
//  Created by Юрий on 20.09.2018.
//  Copyright © 2018 Юрий. All rights reserved.
//

import UIKit

enum CalcStatus {
    //case nothing
    case plus
    case sub
    case mul
    case div
    case ext
}

class ViewController: UIViewController {
    var hasDot = false
    var didCalc = true
    var doEdit = true
    var previousNum = 0.0
    var currentNum = 0.0
    var calcStatus: CalcStatus = .plus

    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func tapButton(sender: AnyObject) {
        guard let button = sender as? UIButton else {
            return
        }
        switch button.tag {
        case 0...9: // All Numbers
            if resultLabel.text == "0" || doEdit {
                resultLabel.text = ""
                doEdit = false
                didCalc = true
            }
            resultLabel.text = resultLabel.text! + button.titleLabel!.text!
            
        case 10: // Button .
            if doEdit {
                resultLabel.text = "0"
                doEdit = false
                didCalc = true
            }
            if !hasDot {
                    resultLabel.text = resultLabel.text! + button.titleLabel!.text!
                    hasDot = true
            }
            
        case 11: // Button =
            if didCalc {
                getResult()
            }
            doEdit = true
            didCalc = false
            
        case 13: // Button AC
            resultLabel.text = "0"
            previousNum = 0.0
            currentNum = 0.0
            hasDot = false
            didCalc = true
            doEdit = true
            calcStatus = .plus
            
        case 14: // Button +/-
            currentNum = Double(resultLabel.text!)!
            currentNum *= -1
            resultLabel.text = String(currentNum)
            
        case 16: // Button <-
            var str = resultLabel.text!
            _ = str.removeLast()
            currentNum /= 10
            if str.count == 0 {
                resultLabel.text = "0"
            }
            else {
                resultLabel.text = str
            }
            
        case 17...20, 15: // Buttons + - * / ^
            if didCalc && !doEdit {
                getResult()
            }
            
            hasDot = false
            doEdit = true
            
            switch button.titleLabel!.text {
            case "+":
                calcStatus = .plus
            case "-":
                calcStatus = .sub
            case "*":
                calcStatus = .mul
            case "/":
                calcStatus = .div
            case "^":
                calcStatus = .ext
            default:
                print(button.titleLabel!.text!)
            }
            
        default:
            print(button.tag)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func getResult() {
        previousNum = Double(resultLabel.text!)!
        switch calcStatus {
        case .plus:
            currentNum += previousNum
        case .sub:
            currentNum -= previousNum
        case .mul:
            currentNum *= previousNum
        case .div:
            if currentNum != 0 {
                currentNum /= previousNum
            }
        case .ext:
            currentNum = extent(currentNum,previousNum)
        }
        
        let result = Double(round(1000*currentNum)/1000)
        resultLabel.text = String(result)
    }
    
    func extent(_ curNum: Double, _ prevNum: Double) -> Double {
        var resNum = curNum
        var extNum = Int(prevNum)
        while extNum != 1 {
            resNum *= curNum
            extNum -= 1
        }
        return resNum
    }


}

